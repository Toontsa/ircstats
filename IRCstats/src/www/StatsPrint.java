package www;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import channel.Channel;
import conf.Defaults;
import debug.Debug;
import users.*;
import words.UniHandler;

public class StatsPrint {

	public static void printAll() {
		printStats();
		printActivity();
		PrintDaily();
		PrintDailyTable();
		PrintMostNicks();
		printMostWords();
		printMostLinks();
		printMostSmiles();
		PrintChannelActivity();
	}

	public static PrintWriter openWriter(String file) {
		PrintWriter writer = null;
		try {
			writer = new PrintWriter(System.getProperty("user.dir")
					+ "/wwwSite/" + file, "UTF-8");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return writer;
	}

	private static void printStats() {
		PrintWriter writer = openWriter("stats.html");

	}

	public static void printActivity() {
		Debug.printInfo("StatsPrint", "printActivity()", "Start", 4);

		PrintWriter writer = openWriter("dailyChar.html");

		double[] hours = new double[24];
		int total = 0;

		for (int i = 0; i <= 23; i++) {
			hours[i] = UserHandler.getHours(i);
			total += hours[i];
		}

		for (int i = 0; i <= 23; i++) {
			hours[i] = (double) Math.round((hours[i] / total) * 10000) / 100;
		}
		writer.println(Arrays.toString(hours));
		writer.close();
	}

	public static void PrintDaily() {
		Debug.printInfo("StatsPrint", "PrintDaily()", "Start", 4);

		PrintWriter writer = openWriter("dailyTop.html");

		String[] name = new String[Defaults.ActiveNicks];
		String[] lines = new String[Defaults.ActiveNicks];
		String[] question = new String[Defaults.ActiveNicks];
		String[] shout = new String[Defaults.ActiveNicks];
		String[] slaps = new String[Defaults.ActiveNicks];
		String[] slapped = new String[Defaults.ActiveNicks];
		String[] caps = new String[Defaults.ActiveNicks];
		String[] hilight = new String[Defaults.ActiveNicks];
		String[] monologys = new String[Defaults.ActiveNicks];

		for (int i = 0; i <= Defaults.ActiveNicks - 1; i++) {
			name[i] = "\"" + UserHandler.getUser(i).getName() + "\"";
			lines[i] = UserHandler.getUser(i).getTotalLines() + "";
			question[i] = UserHandler.getUser(i).getQuestion() + "";
			shout[i] = UserHandler.getUser(i).getShout() + "";
			slaps[i] = UserHandler.getUser(i).getSlaps() + "";
			caps[i] = UserHandler.getUser(i).getCapsLock() + "";
			slapped[i] = UserHandler.getUser(i).getSlapped() + "";
			hilight[i] = UserHandler.getUser(i).getHilights() + "";
			monologys[i] = UserHandler.getUser(i).getMonologys() + "";
		}

		writer.println(Arrays.toString(name));
		writer.println(Arrays.toString(lines));
		writer.println(Arrays.toString(question));
		writer.println(Arrays.toString(shout));
		writer.println(Arrays.toString(slaps));
		writer.println(Arrays.toString(slapped));
		writer.println(Arrays.toString(caps));
		writer.println(Arrays.toString(hilight));
		writer.println(Arrays.toString(monologys));

		writer.close();
	}

	public static void PrintChannelActivity() {
		Debug.printInfo("StatsPrint", "PrintChannelActivity()", "Start", 4);

		PrintWriter writer = openWriter("channelActivity.html");

		int days = Channel.getDayCount();
		writer.println("[");
		for (int i = 0; i <= days; i++) {
			writer.print("[");
			writer.print(Channel.getMsDate(i) + ",");
			writer.print(Channel.getCount(i));
			writer.print("]");
			if (i < days) {
				writer.println(",");
			}
		}
		writer.println("]");
		writer.close();
	}

	public static void PrintDailyTable() {
		Debug.printInfo("StatsPrint", "PrintDailyTable()", "Start", 4);

		PrintWriter writer = openWriter("topTable.html");

		writer.println("<table>");
		writer.print("<tr>");
		writer.print("<td>#</td> ");
		writer.print("<td>Nick</td> ");
		writer.print("<td>Lines</td> ");
		writer.print("<td>When?</td> ");
		writer.print("<td>Chars In Line</td> ");
		writer.print("<td>Last Seen</td> ");
		writer.print("<td>First Time Seen</td> ");
		writer.print("<td>Random Quote</td> ");
		writer.println("</tr>");

		for (int i = 0; i <= Defaults.ActiveNicks - 1; i++) {

			int[] activeTimes = UserHandler.getUser(i).getActiveHours();
			int hours05 = activeTimes[0] + activeTimes[1] + activeTimes[2]
					+ activeTimes[3] + activeTimes[4] + activeTimes[5];
			int hours611 = activeTimes[6] + activeTimes[7] + activeTimes[8]
					+ activeTimes[9] + activeTimes[10] + activeTimes[11];
			int hours1217 = activeTimes[12] + activeTimes[13] + activeTimes[14]
					+ activeTimes[15] + activeTimes[16] + activeTimes[17];
			int hours1823 = activeTimes[18] + activeTimes[19] + activeTimes[20]
					+ activeTimes[21] + activeTimes[22] + activeTimes[13];
			int total = hours05 + hours611 + hours1217 + hours1823;

			writer.print("<tr>");
			writer.print("<td>" + (i + 1) + "</td> ");
			writer.print("<td>" + UserHandler.getUser(i).getName() + "</td> ");
			writer.print("<td>" + UserHandler.getUser(i).getTotalLines()
					+ "</td> ");

			long pros = Math.round((double) hours05 / total * 60);
			writer.print("<td><center><img src=\"img/blue-h.png\" border=\"0\" width=\"");
			writer.print(pros + "\" height=\"15\" alt=\"" + hours05
					+ "\" title=\"" + hours05 + "\" />");

			pros = Math.round((double) hours611 / total * 60);
			writer.print("<img src=\"img/green-h.png\" border=\"0\" width=\"");
			writer.print(pros + "\" height=\"15\" alt=\"" + hours611
					+ "\" title=\"" + hours611 + "\" />");

			pros = Math.round((double) hours1217 / total * 60);
			writer.print("<img src=\"img/yellow-h.png\" border=\"0\" width=\"");
			writer.print(pros + "\" height=\"15\" alt=\"" + hours1217
					+ "\" title=\"" + hours1217 + "\" />");

			pros = Math.round((double) hours1823 / total * 60);
			writer.print("<img src=\"img/red-h.png\" border=\"0\" width=\"");
			writer.print(pros + "\" height=\"15\" alt=\"" + hours1823
					+ "\" title=\"" + hours1823 + "\" /></center></td>");

			writer.print("<td>" + UserHandler.getUser(i).getCharsInLine()
					+ "</td> ");
			writer.print("<td><time class=\"timeago\" datetime=\""
					+ UserHandler.getUser(i).getLastSeen("yyyy-MM-dd") + "\">"
					+ UserHandler.getUser(i).getLastSeen("dd.MM.yyyy")
					+ "</time></td> ");
			writer.print("<td><time class=\"timeago\" datetime=\""
					+ UserHandler.getUser(i).getFirstTimeSeen("yyyy-MM-dd")
					+ "\">"
					+ UserHandler.getUser(i).getFirstTimeSeen("dd.MM.yyyy")
					+ "</time></td> ");
			writer.print("<td>\"<i>" + UserHandler.getUser(i).getQuote()
					+ "</i>\"</td>");
			writer.println("</tr>");
		}

		writer.println("</table>");

		writer.println("<br /><table>");
		writer.print("<tr><td></td><td colspan=\"2\">");
		writer.print("Listalle ei mahtunut ");
		writer.print(UserHandler.getUserCount() - Defaults.ActiveNicks
				- Defaults.ActiveNicks2);
		writer.println(" irkkaajaa.</td><td></td></tr>");
		for (int i = Defaults.ActiveNicks; i <= Defaults.ActiveNicks
				+ Defaults.ActiveNicks2 - 1; i += 4) {
			writer.print("<tr>");
			writer.print("<td>" + UserHandler.getUser(i).getName() + " ("
					+ UserHandler.getUser(i).getTotalLines() + ") </td> ");
			writer.print("<td>" + UserHandler.getUser(i + 1).getName() + " ("
					+ UserHandler.getUser(i + 1).getTotalLines() + ") </td> ");
			writer.print("<td>" + UserHandler.getUser(i + 2).getName() + " ("
					+ UserHandler.getUser(i + 2).getTotalLines() + ") </td> ");
			writer.print("<td>" + UserHandler.getUser(i + 3).getName() + " ("
					+ UserHandler.getUser(i + 3).getTotalLines() + ") </td> ");
			writer.print("</tr>");
		}
		writer.println("</table>");

		writer.close();
	}

	private static String[] getMost(ArrayList<String[]> list, String data) {

		int ID = 0;
		boolean newData = true;

		if (list.size() == 0) {
			list.add(new String[] { data, "", "0", "", "0" });
		}

		for (int i = 0; i <= list.size() - 1; i++) {
			if (list.get(i)[0].equalsIgnoreCase(data)) {
				ID = i;
				newData = false;
			}
		}

		if (newData) {
			list.add(new String[] { data, "", "0", "", "0" });
			ID = list.size() - 1;
		}
		return list.get(ID);

	}

	public static void PrintMostNicks() {
		Debug.printInfo("StatsPrint", "PrintMostNicks()", "Start", 4);

		ArrayList<String[]> topStats = new ArrayList<String[]>();

		for (int ID = 0; ID <= Defaults.ActiveNicks + Defaults.ActiveNicks2 - 1; ID++) {
			User user = UserHandler.getUser(ID);
			compare(getMost(topStats, "getQuestion"), user.getName(),
					user.getQuestionPros());
			compare(getMost(topStats, "getShout"), user.getName(),
					user.getShoutPros());
			compare(getMost(topStats, "getCapsLock"), user.getName(),
					user.getCapsLockPros());

			compare(getMost(topStats, "getSlaps"), user.getName(),
					user.getSlaps());
			compare(getMost(topStats, "getSlapped"), user.getName(),
					user.getSlapped());

			compare(getMost(topStats, "getSmileHappy"), user.getName(),
					user.getSmileHappy());
			compare(getMost(topStats, "getSmileSad"), user.getName(),
					user.getSmileSad());

			compare(getMost(topStats, "getCharsInLine"), user.getName(),
					user.getCharsInLine());
			compare(getMost(topStats, "getWordsInLine"), user.getName(),
					user.getWordsInLine());

			compare(getMost(topStats, "getKick"), user.getName(),
					user.getKick());
			compare(getMost(topStats, "getKicked"), user.getName(),
					user.getKicked());
			compare(getMost(topStats, "getBan"), user.getName(), user.getBan());

			compare(getMost(topStats, "getMonologys"), user.getName(),
					user.getMonologys());

			compare(getMost(topStats, "getOp"), user.getName(), user.getOp());
			compare(getMost(topStats, "getDeOp"), user.getName(),
					user.getDeOp());

			compare(getMost(topStats, "getModOp"), user.getName(),
					user.getModOp());
			compare(getMost(topStats, "getModDeOp"), user.getName(),
					user.getModDeOp());

			compare(getMost(topStats, "getVoice"), user.getName(),
					user.getVoice());
			compare(getMost(topStats, "getDeVoice"), user.getName(),
					user.getDeVoice());

			compare(getMost(topStats, "getModVoice"), user.getName(),
					user.getModVoice());
			compare(getMost(topStats, "getModDeVoice"), user.getName(),
					user.getModDeVoice());

		}

		PrintWriter writer = openWriter("topList.html");

		Texti.read();

		for (int ID = 0; ID <= topStats.size() - 1; ID++) {
			writer.println(PrintMostNicks(topStats.get(ID)));
			writer.println("<hr class=\"most\" />");
		}

		writer.close();
	}

	private static String PrintMostNicks(String[] data) {
		String first = Texti.getLine(data[0] + "1", data[1], data[2]);
		String second = Texti.getLine(data[0] + "2", data[3], data[4]);
		first = "<div class=\"first\">" + first + "</div>";
		second = "<div class=\"second\">" + second + "</div>";
		return first + second;
	}

	private static void printMostWords() {
		Debug.printInfo("StatsPrint", "printMostWords()", "Start", 4);

		PrintWriter writer = openWriter("mostWords.html");

		writer.println("<table>");
		writer.print("<tr>");
		writer.print("<td>#</td> ");
		writer.print("<td>Word</td> ");
		writer.print("<td>Times sayed</td> ");
		writer.print("<td>Last Said</td> ");
		writer.println("</tr>");

		int lines;
		if (Defaults.MOSTWORDS < UniHandler.getWwwSize()) {
			lines = Defaults.MOSTWORDS - 1;
		} else {
			lines = UniHandler.getWwwSize() - 1;
		}

		for (int i = 0; i <= lines; i++) {
			writer.print("<tr><td>" + (i + 1) + "</td>");
			writer.print("<td>" + UniHandler.getWord(i).getWord() + "</td>");
			writer.print("<td>" + UniHandler.getWord(i).getCount() + "</td>");
			writer.print("<td>" + UniHandler.getWord(i).getLastSaid()
					+ "</td></tr>");
		}
		writer.println("</table>");
		writer.close();
	}

	private static void printMostLinks() {
		Debug.printInfo("StatsPrint", "printMostLinks()", "Start", 4);

		PrintWriter writer = openWriter("mostLinks.html");

		writer.println("<table>");
		writer.print("<tr>");
		writer.print("<td>#</td> ");
		writer.print("<td>Link</td> ");
		writer.print("<td>Times sayed</td> ");
		writer.print("<td>Last Said</td> ");
		writer.println("</tr>");

		int lines;
		if (Defaults.WWWLINKS < UniHandler.getWwwSize()) {
			lines = Defaults.WWWLINKS - 1;
		} else {
			lines = UniHandler.getWwwSize() - 1;
		}

		for (int i = 0; i <= lines; i++) {
			writer.print("<tr><td>" + (i + 1) + "</td>");
			writer.print("<td><a href=\"" + UniHandler.getLink(i).getWord()
					+ "\">" + UniHandler.getLink(i).getWord() + "</a></td>");
			writer.print("<td>" + UniHandler.getLink(i).getCount() + "</td>");
			writer.print("<td>" + UniHandler.getLink(i).getLastSaid()
					+ "</td></tr>");
		}
		writer.print("<td></td><td colspan=\"2\">");
		writer.print("<center>Staseihin ei mahtunut ");
		writer.print(UniHandler.getWwwSize() - lines + " linkki�.</center>");
		writer.println("</td><td></td></tr>");
		writer.println("</table>");
		writer.close();
	}

	private static void printMostSmiles() {
		Debug.printInfo("StatsPrint", "printMostSmiles()", "Start", 4);

		PrintWriter writer = openWriter("mostSmiles.html");

		writer.println("<table>");
		writer.print("<tr>");
		writer.print("<td>#</td> ");
		writer.print("<td>Smile</td> ");
		writer.print("<td>Times sayed</td> ");
		writer.print("<td>Last Said</td> ");
		writer.println("</tr>");

		int lines;
		if (Defaults.MOSTSMILEYS < UniHandler.getWwwSize()) {
			lines = Defaults.MOSTSMILEYS - 1;
		} else {
			lines = UniHandler.getWwwSize() - 1;
		}

		for (int i = 0; i <= lines; i++) {
			writer.print("<tr><td>" + (i + 1) + "</td>");
			writer.print("<td>" + UniHandler.getSmile(i).getWord() + "</td>");
			writer.print("<td>" + UniHandler.getSmile(i).getCount() + "</td>");
			writer.print("<td>" + UniHandler.getSmile(i).getLastSaid()
					+ "</td></tr>");
		}
		writer.print("<td></td><td colspan=\"2\">");
		writer.print("<center>Staseihin ei mahtunut ");
		writer.print(UniHandler.getWwwSize() - lines + " hymi�t�.</center>");
		writer.println("</td><td></td></tr>");
		writer.println("</table>");
		writer.close();
	}

	public static void printStatic(long l, int i) {
		Debug.printInfo("StatsPrint", "printMostWords()", "Start", 4);
		PrintWriter writer = openWriter("stats.html");
		writer.println("Logissa yhteens� " + i + " rivi� teksti�.");
		
		String time = String.format(
				"%d min, %d sec",
				TimeUnit.MILLISECONDS.toMinutes(l),
				TimeUnit.MILLISECONDS.toSeconds(l)
						- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS
								.toMinutes(l)));
		writer.println("Statsien generointiin kului: " + time);
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd\'T\'HH:mm");
		DateFormat dateFormat2 = new SimpleDateFormat("dd.MM.yyyy");
		Date date = new Date(System.currentTimeMillis());
		writer.println("Statit p�ivitetty viimeksi <time class=\"timeago\" datetime=\""
				+ dateFormat.format(date) + "\">"
				+ dateFormat2.format(date)
				+ "</time>");
		writer.println(((char)169) + " Tonzaw @QuakeNet & IRCnet");
		writer.close();
		
	}

	private static String[] compare(String data[], String name, int value) {
		/*
		 * 0 = dataType 1 = First name 2 = First value 3 = Second name 4 =
		 * Second value
		 */

		if (value > Integer.valueOf(data[4])) {
			if (value > Integer.valueOf(data[2])) {
				data[3] = data[1];
				data[4] = data[2];
				data[1] = name;
				data[2] = value + "";

			} else {
				data[3] = name;
				data[4] = value + "";
			}
		}
		return data;

	}

	private static String[] compare(String data[], String name, double value) {
		/*
		 * 0 = dataType 1 = First name 2 = First value 3 = Second name 4 =
		 * Second value
		 */

		if (value > Double.valueOf(data[4])) {
			if (value > Double.valueOf(data[2])) {
				data[3] = data[1];
				data[4] = data[2];
				data[1] = name;
				data[2] = value + "";

			} else {
				data[3] = name;
				data[4] = value + "";
			}
		}
		return data;

	}

}
