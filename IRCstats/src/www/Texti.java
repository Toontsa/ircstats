package www;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class Texti {

	private static ArrayList<String> list = new ArrayList<String>();

	public static void read() {
		String line = "a";
		BufferedReader reader2 = null;

		try {
			reader2 = new BufferedReader(new FileReader("lang/fin.txt"));
		} catch (FileNotFoundException e) {
			System.out.println("LogRead() - Cannot open file");
			e.printStackTrace();
		}
		while (!(line == null)) {
			try {
				line = reader2.readLine();
				if(line==null) break;
				if (!line.equals("")) {
					list.add(line);
				}
			} catch (IOException e) {
				System.out.println("readLine()");
				e.printStackTrace();
			}
		}
	}

	public static String getLine(String data, String nick, String value) {
		String string = value + " not found";
		try {
			if (Integer.valueOf(value) == 0) {
				data = data.substring(0, data.length() - 1) + "3";
			}
		} catch (NumberFormatException e) {
			if (Double.valueOf(value) == 0) {
				data = data.substring(0, data.length() - 1) + "3";
			}
		}
		for (int i = 0; i <= list.size() - 1; i++) {
			String line = list.get(i);
			if (line == null) {
				return data + " not found :(";
			}
			if (line.indexOf('=') != -1) {
				if (line.substring(0, line.indexOf('=') - 1).equals(data)) {
					string = line.substring(line.indexOf('=') + 2)
							.replaceAll("NICK", nick)
							.replaceAll("VALUE", value);
					return string;
				}
			}
		}
		return string;

	}
}
