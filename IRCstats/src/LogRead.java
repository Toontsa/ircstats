import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;


public class LogRead {
	
	BufferedReader reader;
	
	public LogRead() {

			 Charset utf8 = Charset.forName("UTF-8");     
			try {
				reader = new BufferedReader(new InputStreamReader(new FileInputStream("logs/iso.log"),utf8));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	
	public String readLine() {
		String line = null;
		try {
			line = reader.readLine();
			//System.out.println(line);
		} catch (IOException e) {
			System.out.println("readLine()");
			e.printStackTrace();
		}
		
		return line;
	}
	
	

}
