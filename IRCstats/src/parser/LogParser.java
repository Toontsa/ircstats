package parser;

import debug.Debug;
import users.*;
import words.UniHandler;

public abstract class LogParser {

	private static String lastNick;
	private static int counter;

	public abstract void parseLine(String line);

	public static void addContent(String nick, int hour, String line) {

		User user = UserHandler.getUser(nick);
		user.setActiveHours(hour);
		user.setTotalLines();
		user.setTotalChars(line.replaceAll(" ", "").length());

		if (user.getName().equalsIgnoreCase(lastNick)) {
			counter++;
		} else {
			lastNick = user.getName();
			counter = 0;
		}

		if (counter > 4) {
			user.setMonologys();
			counter = 0;
		}

		if (line.indexOf('!') != -1 && line.indexOf('!') != 1) {
			user.setShout();
		}
		if (line.indexOf('?') != -1 && line.indexOf('?') != 1) {
			user.setQuestion();
		}
		if (line.matches("([A-Z���]|\\p{Space}|\\p{Punct})*")) {
			user.setCapsLock();
		}

		lineInspect(user, line);
		user.setLastSeen();
		if (user.getQuote() == null || ((int) (Math.random() * 100)) == 66
				&& line.length() > 10) {
			user.setQuote(line);
		}

	}

	private static void lineInspect(User user, String line) {

		String[] lineT = line.split(" ");
		user.setTotalWords(lineT.length);

		for (int i = 0; i <= lineT.length - 1; i++) {
			while (true) {
				if (checkSmile(user, lineT[i])) {
					break;
				}

				if (checkWww(user, lineT[i])) {
					break;
				}

				if (UserHandler.checkHilight(lineT[i], user.getName())) {
					break;
				}
				try {
					UniHandler.getWord(lineT[i]).setSaid(user.getName());
				} catch (java.lang.NullPointerException e) {
					Debug.printInfo("LogParser", "lineInspect",
							"Too short word", 4);
				}
				break;

			}
		}
	}

	private static boolean checkSmile(User user, String word) {
		word = word.replace("?", "").replace("!", "");
		if (word.matches("[:;=8][)>Dd\\]](.*)")) {
			UniHandler.getSmile(word).setSaid(user.getName());
			user.setSmileHappy();
			return true;
		} else if (word.matches("[:;=8][(<\\[](.*)")) {
			UniHandler.getSmile(word).setSaid(user.getName());
			user.setSmileSad();
			return true;
		} else if (word.matches("[:;=8][Oo|]|\\[o]\\/|D:|[.-^oO][_][.-^oO]|[-_oO][.][oO-_]")) {
			UniHandler.getSmile(word).setSaid(user.getName());
			return true;
		} else {
			return false;
		}
	}

	private static boolean checkWww(User user, String word) {
		if (word.contains("www.") || word.contains("http:")
				|| word.contains("https:")) {
			UniHandler.getWww(word).setSaid(user.getName());
			return true;
		} else {
			return false;
		}
	}

	// private static void lineInspect(User user, String line) {
	// user.setTotalLines();
	// user.setTotalChars(line.length());
	//
	// String word;
	// int wordCount = 0;
	//
	// while (line.length() > 0) {
	// try {
	// word = line.substring(0, line.indexOf(' '));
	//
	// if (word.length() > 1) {
	// line = line.substring(line.indexOf(' ') + 1);
	// wordCount++;
	// } else {
	// line = line.substring(line.indexOf(' ') + 1);
	// }
	// } catch (StringIndexOutOfBoundsException e) {
	// word = line;
	// wordCount++;
	// line = "";
	// }
	//
	// if (word.contains("www.") || word.contains("http:")
	// || word.contains("https:")) {
	// WordsHandler.getWord(word, false).setSaid(user.getName());
	// } else {
	//
	// String tempWord = ""; //TODO KORJAA HL
	// for (int i = 0; i <= word.length() - 1; i++) {
	// if (Character.isLetter(word.charAt(i))) {
	// tempWord += word.charAt(i);
	// }
	// }
	//
	// if (word.length() >= 2) {
	// if (!(UserHandler.addHilight(word, user.getName()))) {
	// word = tempWord;
	// WordsHandler.getWord(word, true)
	// .setSaid(user.getName());
	// }
	// }
	// checkSmile(word, user);
	// }
	// }
	// user.setTotalWords(wordCount);
	//
	// }
}
