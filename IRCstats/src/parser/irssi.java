package parser;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import channel.Channel;
import debug.Debug;
import users.UserHandler;

public class irssi extends LogParser {

	public void parseLine(String line) {

		/*
		 * http://www.vogella.com/tutorials/JavaRegularExpressions/article.html
		 * http://docs.oracle.com/javase/7/docs/api/java/util/regex/Pattern.html
		 */

		if (line.matches("^(\\d+):\\d+[^<*^!]+<[@%+~& ]?([^>]+)> (.*)")) {
			normalLine(line);
			Channel.lineCount();

		} else if (line.matches("^(\\d+):\\d+[^ ]+ +\\* (\\S+) (.*)")) {
			actionLine(line);
			Channel.lineCount();

		} else if (line
				.matches("^(\\d+):(\\d+)[^-]+-\\!- (\\S+) (\\S+) (\\S+) (\\S+) (\\S+)(.*)")) {
			systemLine(line);
			
		} else if (line.matches("^[-][-][-](.*)")) {
			dayLine(line);
		}
	}

	public void normalLine(String line) {

		int hour;
		int min;
		String nick;
		String time;

		time = line.substring(0, line.indexOf(' '));
		line = line.substring(line.indexOf(' '));

		hour = Integer.parseInt(time.substring(0, time.indexOf(':')));
		min = Integer.parseInt(time.substring(time.indexOf(':') + 1));

		nick = line.substring(line.indexOf('<') + 2,
				line.indexOf('>') - line.indexOf('<') + 1);

		line = line.substring(line.indexOf('>') + 2);

		Debug.printInfo("irssi", "normalLine", hour + ":" + min + " " + nick
				+ " " + line, 6);

		addContent(nick, hour, line);
	}

	private void systemLine(String line) {
		int hour;
		int min;
		String time;

		time = line.substring(0, line.indexOf(' '));
		line = line.substring(line.indexOf(' '));

		hour = Integer.parseInt(time.substring(0, time.indexOf(':') - 1));
		min = Integer.parseInt(time.substring(time.indexOf(':') + 1));

		line = line.substring(line.indexOf(' ', 2) + 1);

		String[] lineT = line.split(" ");

		if (lineT[3].equalsIgnoreCase("joined")) {
			UserHandler.getUser(lineT[0]).setJoins();
			UserHandler.getUser(lineT[0]).setLastSeen();

		} else if (lineT[3].equalsIgnoreCase("known")) {
			UserHandler.getUser(lineT[0]).setNickChange();
			UserHandler.getUser(lineT[0]).setLastSeen();

		} else if (lineT[0].contains("mode") && lineT[1].contains("o")) {
			if (lineT[1].contains("+")) {
				UserHandler.getUser(lineT[lineT.length - 1]).setModOp();
				UserHandler.getUser(
						lineT[2].substring(0, lineT[2].length() - 1)).setOp();

			} else {
				UserHandler.getUser(lineT[lineT.length - 1]).setModDeOp();
				UserHandler.getUser(
						lineT[2].substring(0, lineT[2].length() - 1)).setDeOp();
			}

		} else if (lineT[0].contains("mode") && lineT[1].contains("v")) {
			if (lineT[1].contains("+")) {
				UserHandler.getUser(lineT[lineT.length - 1]).setModVoice();
				UserHandler.getUser(
						lineT[2].substring(0, lineT[2].length() - 1))
						.setVoice();

			} else {
				UserHandler.getUser(lineT[lineT.length - 1]).setModDeVoice();
				UserHandler.getUser(
						lineT[2].substring(0, lineT[2].length() - 1))
						.setDeVoice();

			}
		} else if (lineT[0].contains("mode") && lineT[1].contains("b")) {
			if (lineT[1].contains("+")) {
				UserHandler.getUser(lineT[lineT.length - 1]).setBan();

			} else {
				UserHandler.getUser(lineT[lineT.length - 1]).setDeBan();

			}
		} else if (lineT[2].equalsIgnoreCase("kicked")) {
			UserHandler.getUser(lineT[0]).setKicked();
			UserHandler.getUser(lineT[6]).setKick();
			UserHandler.getUser(lineT[0]).setLastSeen();
			UserHandler.getUser(lineT[6]).setLastSeen();
		}
		if (lineT[0].contains("mode")){
			UserHandler.getUser(lineT[lineT.length - 1]).setLastSeen();
		}

		Debug.printInfo("irssi", "systemLine", hour + ":" + min + " " + "s"
				+ " " + line, 6);

	}

	private void actionLine(String line) {
		int hour;
		int min;
		String time;

		time = line.substring(0, line.indexOf(' '));
		line = line.substring(line.indexOf(' '));

		hour = Integer.parseInt(time.substring(0, time.indexOf(':') - 1));
		min = Integer.parseInt(time.substring(time.indexOf(':') + 1));

		line = line.substring(line.indexOf(' ', 2) + 1);

		String[] lineT = line.split(" ");

		UserHandler.getUser(lineT[0]).setActions();
		if (lineT[1].equalsIgnoreCase("slaps")) {
			UserHandler.getUser(lineT[0]).setSlaps();
			UserHandler.getUser(lineT[2]).setSlapped();
		}
	}

	private void dayLine(String line) {
		String[] lineT = line.split(" ");
		boolean next = false;
		
		DateFormat dayEng = new SimpleDateFormat("MMMM dd yyyy", Locale.US);
		DateFormat dayEng2 = new SimpleDateFormat("dd.MM.yyyy", Locale.US);
		DateFormat dayFin = new SimpleDateFormat("MMMM dd yyyy", new Locale("fi", "FI"));
		Date d = null;
		
		if (lineT[2].equals("changed")) {
			try {
				d = dayEng.parse(lineT[4] + " " + lineT[5] + " " + lineT[6]);
			} catch (Exception e) {
				next = true;
			}
			
			if (next) {
				try {
					d = dayEng2.parse(lineT[4]);
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
			
		} else if (lineT[2].equals("opened") || lineT[2].equals("closed")) {
			try {
				d = dayEng.parse(lineT[4] + " " + lineT[5] + " " + lineT[7]);
			} catch (ParseException e) {
				next = true;
			}
			
			if(next){
				try {
					d = dayFin.parse(lineT[4] + " " + lineT[5] + " " + lineT[7]);
				} catch (ParseException e) {
					e.printStackTrace();
					next = true;
				}
			}
		}
		Channel.checkDay(d);
	}
	
}
