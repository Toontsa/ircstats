import channel.Channel;
import debug.Debug;
import parser.*;
import users.*;
import words.*;
import www.*;

public class StatsGenerator {

	public static void main(String[] args) {
		
		long startTime = System.currentTimeMillis();
		
		LogRead log = new LogRead();
		LogParser parser = new irssi();
		String line;
		int i=0;
		while ((line = log.readLine()) != null) {
			parser.parseLine(line);
			i++;
			if(i%25000==0){
//				System.out.println("sort");
//				System.out.println(i);
//				UniHandler.SortAll();
//				UserHandler.sortUsers();
				UniHandler.eraseWords();
				System.out.println(i);

			}
		}
		Debug.printInfo("StatsGenerator", "Log Readed",(System.currentTimeMillis()-startTime)+"",1);
		
		UniHandler.SortAll();
		UserHandler.sortUsers();
		Debug.printInfo("StatsGenerator", "Data sorted",(System.currentTimeMillis()-startTime)+"",1);
		
		UserHandler.printUsers();
		StatsPrint.printAll();
		StatsPrint.printStatic(System.currentTimeMillis()-startTime, i);
		Debug.printInfo("StatsGenerator", "Data Printed",(System.currentTimeMillis()-startTime)+"",1);
		
		Debug.printInfo("StatsGenerator", "Completed in time",(System.currentTimeMillis()-startTime)+"",1);
		
		Channel.printDays();
	}
	
}
