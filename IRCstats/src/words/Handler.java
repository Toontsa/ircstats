package words;

import java.util.ArrayList;
import java.util.Collections;

import conf.Defaults;

public class Handler {
	protected static void print(String text, int lineLong) {
		System.out.print(text);
		int len;
		if ((len = lineLong - text.length()) > 0) {
			for (int i = 0; i < len; i++) {
				System.out.print(" ");
			}
		}
	}

	protected static void print(int number, int lineLong) {
		String text = number + "";
		print(text, lineLong);
	}

}
