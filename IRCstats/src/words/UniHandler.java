package words;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class UniHandler {
	private static ArrayList<Words> wwwList = new ArrayList<Words>();
	private static ArrayList<Words> wordList = new ArrayList<Words>();
	private static ArrayList<Words> smileList = new ArrayList<Words>();

	public static Words get(String word, ArrayList<Words> list) {
		int ID = 0;
		boolean newWord = true;

		if (list.size() == 0) {
			list.add(new Words(word));
		}

		for (int i = 0; i <= list.size() - 1; i++) {
			if (list == smileList) {
				if (list.get(i).getWord().equals(word)) {
					ID = i;
					newWord = false;
					break;
				}
			} else {
				if (list.get(i).getWord().equalsIgnoreCase(word)) {
					ID = i;
					newWord = false;
					break;
				}
			}
		}

		if (newWord) {
			list.add(new Words(word));
			ID = list.size() - 1;
		}
		return list.get(ID);
	}

	public static Words getSmile(String word) {
		return get(word, smileList);
	}

	public static Words getWww(String word) {
		return get(word, wwwList);
	}

	public static Words getWord(String word) {
		if ((word = cleanLine(word)) == null) {
			return null;
		} else {
			return get(word, wordList);
		}

	}

	public static Words getWord(int ID) {
		return wordList.get(ID);
	}

	public static Words getLink(int ID) {
		return wwwList.get(ID);
	}

	public static Words getSmile(int ID) {
		return smileList.get(ID);
	}

	public static int getWordSize() {
		return wordList.size();
	}

	public static int getWwwSize() {
		return wwwList.size();
	}

	public static int getSmileSize() {
		return smileList.size();
	}

	public static void SortAll() {
		Collections.sort(wordList, new SortList());
		Collections.sort(wwwList, new SortList());
		Collections.sort(smileList, new SortList());
	}
	public static void eraseWords() {
		int t = 0;
		for (int i = 0; i <= wordList.size() - 1; i++) {
			t+=wordList.get(i).getCount();
		}
		System.out.println();
		System.out.println(wordList.size());
		t = ((int)(t/wordList.size()));
		System.out.println(t);
		int i=0;
		while(i <= wordList.size() - 1){
			if(wordList.get(i).getCount()<=t){
				wordList.remove(i);
			}else{
				i++;
			}
		}
	}

	/**
	 * Poistaa erikoismerkit
	 * 
	 * @param word
	 *            Sana jota haetaan.
	 * @return String putsattu.
	 */
	private static String cleanLine(String word) {
		word = word.replaceAll("[^A-Za-z������]", "");
		if (word.length() > 3) {
			return word;
		} else {
			return null;
		}
	}

	private static class SortList implements Comparator<Words> {
		public int compare(Words o1, Words o2) {
			return Integer.compare(o2.getCount(), o1.getCount());
		}
	}

}
