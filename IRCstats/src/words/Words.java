package words;

public class Words {
	
	public String word;
	private int count;
	private String lastSaid="";
	
	public Words(String word){
		this.word = word;
	}
	
	public String getWord() {
		return word;
	}
	
	public int getCount() {
		return count;
	}

	public String getLastSaid() {
		return lastSaid;
	}

	public void setSaid(String lastSaid) {
		this.lastSaid = lastSaid;
		count++;
	}

}
