package debug;
import java.util.Date;

public class Debug {

    
    //Debug level;
	public static final int DEBUG = 1;
	
	
  /**
   * Outputs to console given information.
   * 
   * Outputs to console the given class name, method name and message if
   * the given level is smaller than the DEBUG level.
   * 
   * @param className name of the class that invokes this method
   * @param methodName name of the method that invokes this method
   * @param msg message to be printed
   * @param level level of the debug information
   */
  public static void printInfo(String className, String methodName, String msg, int level) {
		if(level <= DEBUG ){
			Date date = new Date();
			System.out.print(date + " ");
			System.out.println(className + "." + methodName + ": " + msg);
		}
  }
}
