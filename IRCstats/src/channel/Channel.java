package channel;

import java.util.ArrayList;
import java.util.Date;

import users.User;

public class Channel {
	private static ArrayList<ChanDay> dayList = new ArrayList<ChanDay>();
	
	private static Date currentDate;
	private static int lineCount;
	
	public static void checkDay(Date date){
		if(currentDate==null){
			currentDate=date;
		}
		lineCount++;
		if(date.after(currentDate)){
			dayList.add(new ChanDay(currentDate, lineCount));
			currentDate=date;
			lineCount=0;
		}
		
	}
	
	public static void lineCount(){
		lineCount++;
	}
	
	public static void printDays(){
		for(int i=0; i<=dayList.size()-1; i++){
			System.out.print(dayList.get(i).day());
			System.out.print(" -- ");
			System.out.println(dayList.get(i).count());
		}
	}
	
	public static Date getDate() {
		return currentDate;
	}
	
	public static long getMsDate(int i) {
		return dayList.get(i).day().getTime();
	}
	public static int getCount(int i) {
		return dayList.get(i).count();
	}
	public static int getDayCount(){
		return dayList.size()-1;
	}
}
