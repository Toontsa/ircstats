package channel;

import java.util.Date;

public class ChanDay {
	Date date;
	int count;
	
	public ChanDay(Date date, int count){
		this.date = date;
		this.count = count;
	}
	
	public Date day(){
		return date;
	}
	
	public int count(){
		return count;
	}
}
