package users;

import java.text.SimpleDateFormat;
import java.util.Date;

import channel.Channel;
import conf.Alias;
import debug.Debug;

public class User {

	private String name;
	private int totalLines;
	private int totalWords;
	private long totalChars;
	private int[] activeHours;
	
	private Date lastSeen;
	private Date firstTimeSeen;
	private String quote;

	private int question;
	private int shout;
	private int capsLock;
	private int slaps;
	private int slapped;
	private int smileHappy;
	private int smileSad;

	private int hilights;
	private String lastHilight;

	private int kick;
	private int kicked;
	private int ban;
	private int deBan;

	private int op;
	private int deop;
	private int modOp;
	private int modDeOp;

	private int voice;
	private int devoice;
	private int modVoice;
	private int modDeVoice;

	private int actions;
	private int monologys;
	private int joins;
	private int nickChange;
	private int swearword;
	
	public User(String name) {
		this.name = name;
		activeHours = new int[24];
		firstTimeSeen = Channel.getDate();
		Debug.printInfo("User", "User()", "new user: " + name, 5);
	}

	public String getName() {
		return name;
	}
	public String getQuote() {
		return quote;
	}
	public void setQuote(String line) {
		quote = line;
	}
	
	public void setLastSeen(){
		lastSeen = Channel.getDate();
	}
	
	public String getLastSeen(String format){
		SimpleDateFormat dateFormat = new SimpleDateFormat(format);
		return dateFormat.format(lastSeen);
	}
	
	public String getFirstTimeSeen(String format){
		SimpleDateFormat dateFormat = new SimpleDateFormat(format);
		return dateFormat.format(firstTimeSeen);
	}

	public void setTotalWords(int totalWords) {
		this.totalWords += totalWords;
	}

	public int getTotalWords() {
		return totalWords;
	}

	public void setShout() {
		shout++;
	}

	public int getShout() {
		return shout;
	}
	
	public double getShoutPros() {
		double number = (double) shout / totalLines;
		int round = (int) Math.round(number * 1000);
		number = round / 100.0;
		return number;
	}

	public void setQuestion() {
		question++;
	}

	public int getQuestion() {
		return question;
	}
	
	public double getQuestionPros() {
		double number = (double) question / totalLines;
		int round = (int) Math.round(number * 1000);
		number = round / 100.0;
		return number;
	}

	public void setTotalLines() {
		totalLines++;
	}

	public int getTotalLines() {
		return totalLines;
	}

	public void setActiveHours(int a) {
		activeHours[a]++;
	}

	public int[] getActiveHours() {
		return activeHours;
	}
	
	public int getActiveHour(int a) {
		return activeHours[a];
	}
	

	public void setTotalChars(int a) {
		totalChars += a;
	}

	public long getTotalChars() {
		return totalChars;
	}

	public double getWordsInLine() {
		double number = (double) totalWords / totalLines;
		int round = (int) Math.round(number * 100);
		number = round / 100.0;
		return number;
	}

	public double getCharsInLine() {
		double number = (double) totalChars / totalLines;
		int round = (int) Math.round(number * 100);
		number = round / 100.0;
		return number;
	}

	public int getHilights() {
		return hilights;
	}

	public void setCapsLock() {
		capsLock++;
	}

	public int getCapsLock() {
		return capsLock;
	}
	
	public double getCapsLockPros() {
		double number = (double) capsLock / totalLines;
		int round = (int) Math.round(number * 100);
		number = round / 100.0;
		return number;
	}

	public String getLastHilight() {
		if (lastHilight == null) {
			return "";
		}
		return lastHilight;
	}

	public void setHilight(String lastHilight) {
		this.lastHilight = lastHilight;
		hilights++;
	}

	public int getSmileHappy() {
		return smileHappy;
	}

	public void setSmileHappy() {
		smileHappy++;
	}

	public int getSmileSad() {
		return smileSad;
	}

	public void setSmileSad() {
		smileSad++;
	}

	public int getJoins() {
		return joins;
	}

	public void setJoins() {
		joins++;
	}

	public int getNickChange() {
		return nickChange;
	}

	public void setNickChange() {
		nickChange++;
	}

	public void setOp() {
		op++;
	}

	public int getOp() {
		return op;
	}

	public void setDeOp() {
		deop++;
	}

	public int getDeOp() {
		return deop;
	}

	public void setModOp() {
		modOp++;
	}

	public int getModOp() {
		return modOp;
	}

	public void setModDeOp() {
		modDeOp++;
	}

	public int getModDeOp() {
		return modDeOp;
	}

	public void setVoice() {
		voice++;
	}

	public int getVoice() {
		return voice;
	}

	public void setDeVoice() {
		devoice++;
	}

	public int getDeVoice() {
		return devoice;
	}

	public void setModVoice() {
		modVoice++;
	}

	public int getModVoice() {
		return modVoice;
	}

	public void setModDeVoice() {
		modDeVoice++;
	}

	public int getModDeVoice() {
		return modDeVoice;
	}

	public void setKick() {
		kick++;
	}

	public int getKick() {
		return kick;
	}

	public void setKicked() {
		kicked++;
	}

	public int getKicked() {
		return kicked;
	}

	public void setBan() {
		ban++;
	}

	public int getBan() {
		return ban;
	}

	public void setDeBan() {
		deBan++;
	}

	public int getDeBan() {
		return deBan;
	}

	public void setSlaps() {
		slaps++;
	}

	public int getSlaps() {
		return slaps;
	}

	public void setSlapped() {
		slapped++;
	}

	public int getSlapped() {
		return slapped;
	}

	public void setActions() {
		actions++;
	}

	public int getActions() {
		return actions;
	}

	public void setMonologys() {
		monologys++;
	}

	public int getMonologys() {
		return monologys;
	}

	public void setSwearword() {
		swearword++;
	}

	public int getSwearword() {
		return swearword;
	}

	public void merge(User alias) {
		Debug.printInfo("User", "merge",name + " and alias: " + alias.getName(),4);
		totalLines += alias.getTotalLines();
		totalWords += alias.getTotalWords();
		totalChars += alias.getTotalChars();
		// lastSeen;
		// firstTimeSeen;

		question += alias.getQuestion();
		shout += alias.getShout();
		capsLock += alias.getCapsLock();
		slaps += alias.getSlaps();
		slapped += alias.getSlapped();
		smileHappy += alias.getSmileHappy();
		smileSad += alias.getSmileSad();

		hilights += alias.getHilights();
		// protected String lastHilight;

		kick += alias.getKick();
		kicked += alias.getKicked();
		ban += alias.getBan();
		deBan += alias.getDeBan();

		op += alias.getOp();
		deop += alias.getDeOp();
		modOp += alias.getModOp();
		modDeOp += alias.getModDeOp();

		voice += alias.getVoice();
		devoice += alias.getDeVoice();
		modVoice += alias.getModVoice();
		modDeVoice += alias.getModDeVoice();

		actions += alias.getActions();
		monologys += alias.getMonologys();
		joins += alias.getJoins();
		nickChange += alias.getNickChange();
		swearword += alias.getSwearword();
		
		for(int i=0; i<=activeHours.length-1; i++){
			activeHours[i]=alias.getActiveHour(i);
		}
	}
}