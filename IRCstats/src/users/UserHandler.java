package users;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import conf.Defaults;

public class UserHandler {

	private static ArrayList<User> userList = new ArrayList<User>();
	private static int userCount;

	public static User getUser(String name) {

		int ID = 0;
		boolean newUser = true;

		if (userList.size() == 0) {
			userList.add(new User(name));
		}

		for (int i = 0; i <= userList.size() - 1; i++) {
			if (userList.get(i).getName().equalsIgnoreCase(name)) {
				ID = i;
				newUser = false;
				break;
			}
		}

		if (newUser) {
			userList.add(new User(name));
			ID = userList.size() - 1;
		}

		return userList.get(ID);
	}

	public static User getUser(int ID) {
		return userList.get(ID);
	}

	public static int getUserCount() {
		return userList.size();
	}

	public static boolean checkHilight(String name, String lastHilight) {
		for (int i = 0; i <= userList.size() - 1; i++) {
			if (name.contains(userList.get(i).getName())) {
				if (lastHilight == userList.get(i).getName()) {
					return false;
				} else {
					userList.get(i).setHilight(lastHilight);
					return true;
				}
			}
		}
		return false;

	}

	public static void printUsers() {
		// checkUserAlias();

		System.out.println("- - -");
		print("Nick", 15);
		print("Lines", 6);
		print("Words", 6);
		print("Chars", 6);
		print("Quest", 6);
		print("Shout", 6);
		print("CAPS", 6);
		print("Hilight", 8);
		print("LastHl", 15);
		print("Words IL", 9);
		print("Chars IL", 9);
		print(":)", 5);
		print(":(", 5);
		print("Seen", 32);
		print("First", 32);
		System.out.println();

		int lines = Defaults.ActiveNicks;
		if (lines == -1 || Defaults.ActiveNicks > userList.size()) {
			lines = userList.size();
		}

		for (int i = 0; i <= Defaults.ActiveNicks; i++) {
			print(userList.get(i).getName(), 15);
			print(userList.get(i).getTotalLines(), 6);
			print(userList.get(i).getTotalWords(), 6);
			print(userList.get(i).getTotalChars(), 6);
			print(userList.get(i).getQuestion(), 6);
			print(userList.get(i).getShout(), 6);
			print(userList.get(i).getCapsLock(), 6);
			print(userList.get(i).getHilights(), 8);
			print(userList.get(i).getLastHilight(), 15);
			print(userList.get(i).getWordsInLine(), 9);
			print(userList.get(i).getCharsInLine(), 9);
			print(userList.get(i).getSmileHappy(), 5);
			print(userList.get(i).getSmileSad(), 5);
			print(userList.get(i).getLastSeen("yyyy-MM-dd"), 32);
			print(userList.get(i).getFirstTimeSeen("yyyy-MM-dd"), 32);
			System.out.println();

		}
		System.out.println("- - -");
	}

	private static void print(String text, int lineLong) {
		System.out.print(text);
		int len;
		if ((len = lineLong - text.length()) > 0) {
			for (int i = 0; i < len; i++) {
				System.out.print(" ");
			}
		}
	}

	private static void print(int number, int lineLong) {
		String text = number + "";
		print(text, lineLong);
	}

	private static void print(long number, int lineLong) {
		String text = number + "";
		print(text, lineLong);
	}

	private static void print(double number, int lineLong) {
		int round = (int) Math.round(number * 100);
		number = round / 100.0;
		String text = number + "";
		print(text, lineLong);
	}

	private static void print(int[] table, int lineLong) {
		String text = "";
		for (int i = 0; i <= table.length - 1; i++) {
			text += table[i];
			if (i <= table.length - 2) {
				text += "|";
			}
		}
		print(text, lineLong);
	}

	private static void print(Date date, int lineLong) {
		print(date + " ", lineLong);
	}

	private static void checkUserAlias() {
		String nick = "Tounuli";
		String alias = "Tounuli_";
		for (int i = 0; i <= userList.size() - 1; i++) {
			if (userList.get(i).getName().equalsIgnoreCase(nick)) {
				for (int z = 0; z <= userList.size() - 1; z++) {
					if (userList.get(z).getName().equalsIgnoreCase(alias)) {
						userList.get(i).merge(userList.get(i - 1));
						userList.remove(z);
					}
				}
			}
		}
	}

	public static void sortUsers() {
		Collections.sort(userList, new UserCompare("getLineCount"));
	}

	static class UserCompare implements Comparator<User> {
		String method;

		public UserCompare(String string) {
			method = string;
		}

		public int compare(User o1, User o2) {
			if (method == "getLineCount") {
				return Integer.compare(o2.getTotalLines(), o1.getTotalLines());
			} else {
				return Integer.compare(o2.getTotalLines(), o1.getTotalLines());
			}
		}
	}

	public static int getHours(int a) {
		int total = 0;
		for (int i = 1; i <= userList.size(); i++) {
			total += userList.get(i - 1).getActiveHour(a);
		}
		return total;
	}
}
